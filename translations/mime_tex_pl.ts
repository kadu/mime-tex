<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="pl">
<defaultcodec></defaultcodec>
<context>
    <name>@default</name>
    <message>
        <source>TeX formula support</source>
        <translation type="obsolete">Wsparcie dla formuł TeX-owych</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="1"/>
        <source>Formula refreshment interval</source>
        <translation>Częstotliwość odświeżania obrazka</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="2"/>
        <source>Default font size</source>
        <translation>Domyślna wielkość czcionki</translation>
    </message>
    <message>
        <source>tiny</source>
        <translation type="obsolete">Maleńka</translation>
    </message>
    <message>
        <source>small</source>
        <translation type="obsolete">Mała</translation>
    </message>
    <message>
        <source>normalsize</source>
        <translation type="obsolete">Normalny rozmiar</translation>
    </message>
    <message>
        <source>large</source>
        <translation type="obsolete">Duża</translation>
    </message>
    <message>
        <source>Large (default)</source>
        <translation type="obsolete">Większa (domyślnie)</translation>
    </message>
    <message>
        <source>LARGE</source>
        <translation type="obsolete">Ogromna</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="3"/>
        <source>Remove GIF files on module unload</source>
        <translation>Usuń Obrazki GIF przy wyładowaniu modułu</translation>
    </message>
    <message>
        <source>When this option is set, all formula images will be removed from
disk on module unload (e. g. when exiting Kadu). Note that only
files from the current MimeTeX session will be removed.</source>
        <translation type="obsolete">Gdy ta opcja jest zaznaczona, wszystkie obrazki zostaną usunięte
z dysku podczas syładowywania modułu (m. in. przy zakończeniu Kadu).
Usunięte zostaną obrazki wyłącznie z obecnej sesji MimeTeX.</translation>
    </message>
    <message>
        <location filename="../.configuration-ui-translations.cpp" line="4"/>
        <source>Use transparent background</source>
        <translation>Używaj przezroczystego tła</translation>
    </message>
    <message>
        <source>mimetex binary is able to produce gif images with trasparent
background. It looks ok when the chat window&apos;s background is
in high contrast with the font color.</source>
        <translation type="obsolete">Program mimetex potrafi generować obrazki z przezroczystym
tłem. Wygląda to dobrze gdy kolor tła okna czata jest kontrastowe
w stosunku do koloru czcionki.</translation>
    </message>
</context>
<context>
    <name>MimeTeX::MimeTeX</name>
    <message>
        <location filename="../mime_tex.cpp" line="60"/>
        <source>Insert TeX formula</source>
        <translation>Wstaw formułę TeX</translation>
    </message>
</context>
<context>
    <name>MimeTeX::TeXFormulaDialog</name>
    <message>
        <location filename="../tex_formula_dialog.cpp" line="71"/>
        <source>Formula image</source>
        <translation>Obraz formuły</translation>
    </message>
    <message>
        <location filename="../mime_tex.cpp" line="60"/>
        <source>Insert TeX formula</source>
        <translation type="obsolete">Wstaw formułę TeX</translation>
    </message>
    <message>
        <location filename="../tex_formula_dialog.cpp" line="73"/>
        <source>Components</source>
        <translation>Komponenty</translation>
    </message>
    <message>
        <location filename="../tex_formula_dialog.cpp" line="100"/>
        <source>&amp;Relations</source>
        <translation>&amp;Relacje</translation>
    </message>
    <message>
        <location filename="../tex_formula_dialog.cpp" line="103"/>
        <source>&amp;Greek Letters</source>
        <translation>&amp;Greckie Litery</translation>
    </message>
    <message>
        <location filename="../tex_formula_dialog.cpp" line="106"/>
        <source>&amp;Arrows</source>
        <translation>&amp;Strzałki</translation>
    </message>
    <message>
        <location filename="../tex_formula_dialog.cpp" line="109"/>
        <source>&amp;Delimiters</source>
        <translation>&amp;Oddzielacze</translation>
    </message>
    <message>
        <location filename="../tex_formula_dialog.cpp" line="112"/>
        <source>&amp;Symbols</source>
        <translation>&amp;Symbole</translation>
    </message>
    <message>
        <location filename="../tex_formula_dialog.cpp" line="827"/>
        <source>&amp;Undo</source>
        <translation>&amp;Cofnij</translation>
    </message>
    <message>
        <location filename="../tex_formula_dialog.cpp" line="829"/>
        <source>&amp;Redo</source>
        <translation>&amp;Ponów</translation>
    </message>
    <message>
        <location filename="../tex_formula_dialog.cpp" line="830"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../tex_formula_dialog.cpp" line="831"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <location filename="../tex_formula_dialog.cpp" line="929"/>
        <source>TeX formula creator</source>
        <translation>Kreator formuł TeX-owych</translation>
    </message>
    <message>
        <location filename="../tex_formula_dialog.cpp" line="929"/>
        <source>Unable to run mimetex binary!</source>
        <translation>Nie mogę uruchomić programu mimetex!</translation>
    </message>
</context>
</TS>
